import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
const firebaseConfig = {
  apiKey: "AIzaSyADL4y0AK9H8gf5ceZ-c-n6taIld0UR_q8",
    authDomain: "silicon-stock-350206.firebaseapp.com",
    projectId: "silicon-stock-350206",
    storageBucket: "silicon-stock-350206.appspot.com",
    messagingSenderId: "100585024396",
    appId: "1:100585024396:web:5a3495be9346037ee4c4a9",
    measurementId: "G-5Y0XF3ZKFE"
}
// const firebaseConfig = {
//   apiKey: "AIzaSyCKzXIlxd4gRzTia61hJrW5ZC4sHfz9N5M",
//   authDomain: "instagram-abc09.firebaseapp.com",
//   projectId: "instagram-abc09",
//   storageBucket: "instagram-abc09.appspot.com",
//   messagingSenderId: "162899186561",
//   appId: "1:162899186561:web:2d86794235a68095c54e80",
//   measurementId: "G-M3WNSPQ14Y",
// };
export const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);

export const auth = getAuth(app);