import { ADDUSER, CLEAN_UP, LOG_OUT_SUCCESS, USER_FOLLOWING, USER_POST_STATE_CHANGE } from "../constants"

const initialState = {
    currentUser: {},
    posts : [],
    following : [],
}
export const user = (state = initialState, {type, payload}) =>{
    switch (type) {
        case ADDUSER : 
            return {...state, currentUser: {...payload}}
        case USER_POST_STATE_CHANGE : 
            return {...state, posts: payload}
        case USER_FOLLOWING : {
            return {...state, following: [...payload]}
        }
        case LOG_OUT_SUCCESS : 
            return {...state, currentUser: {}, posts: [], following: []}
        case CLEAN_UP : 
            return {currentUser: {}, posts: [], following: []}
        default : 
            return state
    }
}