import { CLEAN_UP, USERS_DATA_STATE_CHANGE, USERS_POSTS_STATE_CHANGE, FOLLOWING_STORIES_ADD, USERS_LIKES_STATE_CHANGE } from "../constants"

const initialState = {
    usersList: [],
    userLoaded : 0,
    feed : [],
    stories : [],
}
export const users = (state = initialState, {type, payload}) =>{
    switch (type) {
        case USERS_DATA_STATE_CHANGE : {
            // console.log(payload, 'payload in following data')
            return {...state, usersList : [...state.usersList, payload] }
        }
        case FOLLOWING_STORIES_ADD : {
            if(payload.length === 0){
                return state;
            } else {
                let arr = state.stories;
                let flag = false;
                for(let i = 0; i<payload.length; i++){
                    if(!flag){
                        arr.map((item)=>{
                            if(item.id === payload[i].id){
                                flag = true;
                            }
                        })
                    }
                }
                if(flag){
                    return state;
                }
                return {...state,
                    stories : [...state.stories,...payload]
                }
            }
        }
        case USERS_POSTS_STATE_CHANGE : {
            if(payload.length === 0){
                return state;
            } else {
                let arr = state.feed;
                let flag = false;
                for(let i = 0; i<payload.length; i++){
                    if(!flag){
                        arr.map((item)=>{
                            if(item.id === payload[i].id){
                                flag = true;
                            }
                        })
                    }
                }
                if(flag){
                    return state;
                }
                return {...state, userLoaded: state.userLoaded + 1,
                    feed : [...state.feed,...payload]
                }
            }
        }
        case USERS_LIKES_STATE_CHANGE : 
        return {...state,feed : state.feed.map(post => post.id === payload.postId ? {...post, currentUserLike : payload.currentUserLike, uid : payload.uid} : post) }
        case CLEAN_UP : 
        return {usersList : [], userLoaded : 0, feed : [], stories : []}
        default : 
        return state
    }
}
// case USERS_POSTS_STATE_CHANGE : {
//     return {...state, userLoaded: state.userLoaded + 1,
//         usersList : state.usersList.map(user => user.id === payload.uid ? {...user, posts : payload.arr}: user)
//     }
// }