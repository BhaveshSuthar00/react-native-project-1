import { signOut } from "firebase/auth";
import { doc, getDoc, getDocs,collectionGroup,query, where, collection, onSnapshot, orderBy } from "firebase/firestore";
import { auth, db } from "../../firebase";
import { ADDUSER, USER_POST_STATE_CHANGE, CLEAN_UP, USERS_DATA_STATE_CHANGE, USER_FOLLOWING,LOG_OUT_SUCCESS, USERS_POSTS_STATE_CHANGE, USERS_LIKES_STATE_CHANGE, FOLLOWING_STORIES_ADD } from '../constants';
const AddUser = (data) => ({type : ADDUSER, payload : data})
const AddUserPosts = (data) => ({type : USER_POST_STATE_CHANGE, payload : data});
const AddUserFollowing = (data)=> ({type : USER_FOLLOWING, payload : data});
const LogOutSuccess = ()=> ({type : LOG_OUT_SUCCESS})
const AddOtherUsers = (data) => ({type : USERS_DATA_STATE_CHANGE, payload : data});
const Users_Following_State_Change = (data) => ({type : USERS_POSTS_STATE_CHANGE, payload : data});
const Users_Likes_State_Change = (data)=> ({type : USERS_LIKES_STATE_CHANGE, payload : data});
const CleanUp = ()=> ({type : CLEAN_UP});
const AddStories = (data)=> ({type : FOLLOWING_STORIES_ADD, payload : data});
export function clearData(){
    return (dispatch)=>{
        dispatch(CleanUp())
    }
}
export function fetchUser (){
    return (dispatch)=> {
        dispatch(AddUser(auth.currentUser))
    }
}
export function fetchUserPosts (){
    return async(dispatch, getState)=> {
        try {
            const q = query(collection(db, "posts", auth.currentUser.uid, "userPosts"));
            // const q = query(collection(db, "posts", auth.currentUser.uid, "userPosts"), orderBy("dataExample","asc"));
            // const allFollowing = await getDocs(q);
            const arr = [];
            onSnapshot(q, (allFollowing)=>{
                allFollowing.forEach((doc) => { 
                    const data = doc.data();
                    const id = doc.id;
                    arr.push({id , ...data})
                })
                dispatch(AddUserPosts(arr))
            })
        }
        catch (err) {
            console.log(err, 'error in getting user posts for profile')
        }
    }
}
export function fetchUserFollowing (){
    return async(dispatch, getState)=> {
        try {
            // console.log('jere in fetchUserFollowing');
            const q = query(collection(db, "following", auth.currentUser.uid, "userFollowing"))
            onSnapshot(q, (all)=>{    
                const arr = [];
            all.forEach((doc) => { 
                const id = doc.id;
                arr.push(id);
            })
            // console.log('working till here')
            dispatch(AddUserFollowing(arr))
            for(let i = 0; i<arr.length; i++){
                dispatch(fetchUserData(arr[i]))
            }    
        });
        }
        catch(err) {
            console.log(err, 'error in fetchUserFollowing')
        }
    }
}
export const fetchUserData = (uid)=>{
    return async(dispatch, getState)=> {
        try {
            const found = getState().users.usersList;
            let flag = false;
            console.log(found, 'inside another none')
            found.map((item)=> {
                if(item.uid === uid){
                    flag = true;
                    console.log('here in id', flag)
                }
            })
            if(!flag){                
                const docRef = doc(db, "users", uid);
                const docSnap = await getDoc(docRef);
                if (docSnap.exists()) {
                    const id = docSnap.id;
                    const data = docSnap.data();
                    const obj  = {
                        id: id,
                        ...data,
                    };
                    dispatch(AddOtherUsers(obj))
                    dispatch(fetchUsersFollowingPosts(docSnap.id))
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
            }
        }
        catch(err) {
            console.log(err, 'error in fetchUserData')
        }
    }
}
export function fetchUsersFollowingPosts (uid){
    return async(dispatch, getState)=> {
        try {
            // const q = query(collection(db, "posts", uid, "userPosts"), orderBy("dateExample","asc"));
            const q = query(collection(db, "posts", uid, "userPosts"));
            const allFollowing = await getDocs(q);
            const arr = [];
            allFollowing.forEach((doc) => { 
                const data = doc.data();
                const id = doc.id;
                arr.push({id , ...data})
            })
            if(arr) {
                dispatch(Users_Following_State_Change(arr))
            }
            for(let i = 0; i < arr.length; i++) {
                dispatch(fetchUsersFollowingLikes(uid, arr[i].id));
                dispatch(fetchUsersFollowingStories(uid));
            }
        }
        catch(err) {
            console.log(err, 'error in fetchUsersFOllowingPosts')
        }
    }
}
export function fetchUsersFollowingStories (uid){
    return async(dispatch, getState)=> {
        try {
            // const q = query(collection(db, "posts", uid, "userPosts"), orderBy("dateExample","asc"));
            const q = query(collection(db, "posts", uid, "userStories"));
            const allFollowing = await getDocs(q);
            const arr = [];
            allFollowing.forEach((doc) => { 
                const data = doc.data();
                const id = doc.id;
                arr.push({id, uid: uid , ...data})
            })
            if(arr) {
                dispatch(AddStories(arr))
            }
            
        }
        catch(err) {
            console.log(err, 'error in getting stories')
        }
    }
}
export function fetchUsersFollowingLikes (uid, postId){
    return async(dispatch, getState)=> {
        try {
            // console.log('here in following likes function')
            const q = query(collection(db, "posts", uid, "userPosts", postId, "likes"));
            onSnapshot(q, (allFollowing)=>{
                let currentUserLike = false;
                allFollowing.forEach((doc) => {
                    const id = doc.id;
                    if(id === auth.currentUser.uid) {
                        currentUserLike = true;
                    } 
                })
                dispatch(Users_Likes_State_Change({currentUserLike : currentUserLike, postId : postId, uid : uid}))
            })
        }
        catch(err) {
            console.log(err, 'error in fetchUsersFollowingLikes')
        }
    }
}

export const LogOut = ()=>{
    return async(dispatch)=> {
        try {    
            const logout = await signOut(auth)
            dispatch(LogOutSuccess())
        }
        catch(err) {
            console.log('error in logout', err)
        }
    }

}