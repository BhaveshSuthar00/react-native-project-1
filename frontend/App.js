import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Landing from "./components/auth/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./firebase";
import { Provider } from 'react-redux'
import { store } from "./redux/store";
import { Main } from "./components/Main";
import { Add } from "./components/main/Add";
import { Save } from "./components/main/Save";
import { Comment } from "./components/main/Comment";
import StoriesSingle from "./components/main/StoriesSingle";
const Stack = createStackNavigator();
export default function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setLoggedIn(true);
      } else {
        setLoggedIn(false);
      }
    });
  }, []);
  if (!loggedIn) {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Landing">
          <Stack.Screen
            name="Landing"
            component={Landing}
            options={{ headerShown: false }}
          />
            
          <Stack.Screen name="Register" component={Register}  />
          <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Main">
            <Stack.Screen name="Main" component={Main} options={{ headerShown: false }} />
            <Stack.Screen name="MainAdd" component={Add} />
            <Stack.Screen name="Stories" component={StoriesSingle} />
            <Stack.Screen name="Save" component={Save} />
            <Stack.Screen name="Comment" component={Comment} />
          </Stack.Navigator>
      </NavigationContainer>

    </Provider>
  );
}