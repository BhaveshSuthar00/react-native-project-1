import React, { useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useDispatch } from 'react-redux'
import { clearData, fetchUser, fetchUserFollowing, fetchUserPosts } from '../redux/actions';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Profile from './main/Profile'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Search from './main/Search';
import { auth } from '../firebase';
import Feed from './main/Feed';
const Tab =  createMaterialBottomTabNavigator();

const EmptyScreen = ()=> {
    return (null)
}

export const Main = () => {
    const dispatch = useDispatch();
    useEffect(()=> {
        dispatch(clearData())
        dispatch(fetchUser())
        dispatch(fetchUserPosts())
        dispatch(fetchUserFollowing())
    }, [])
    return (
        <Tab.Navigator labeled={false}>
            <Tab.Screen name="Feed" component={Feed} 
                options={{ 
                    headerShown : false,
                    tabBarIcon : ({color, size}) => (
                        <MaterialCommunityIcons name='home' color={color} size={26} />
                    )
                }}
            />
            <Tab.Screen name="Search" component={Search} 
                options={{ 
                    headerShown : false,
                    tabBarIcon : ({color, size}) => (
                        <MaterialCommunityIcons name='magnify' color={color} size={26} />
                    )
                }}
            />
            <Tab.Screen name="Add" component={EmptyScreen}  
                listeners={({navigation}) => ({
                    tabPress: event => {
                        event.preventDefault();
                        navigation.navigate('MainAdd', {type : "Post"});
                    }
                })}
                options={{ 
                    headerShown : false,
                    tabBarIcon : ({color, size}) => (
                        <MaterialCommunityIcons name='plus-box' color={color} size={26} />
                    )
                }}
            />
            <Tab.Screen name="Profile" component={Profile} 
                listeners={({navigation}) => ({
                    tabPress: event => {
                        event.preventDefault();
                        navigation.navigate('Profile', {uid : auth.currentUser.uid});
                    }
                })}
                options={{ 
                    headerShown : false,
                    tabBarIcon : ({color, size}) => (
                        <MaterialCommunityIcons name='account-circle' color={color} size={26} />
                    )
                }}
            />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
    },
    text: {
    color: "white",
    fontSize: 25,
    backgroundColor: "black",
    },
    inputField : {
    color: "white",
    fontSize: 25,
    width: '50%',
    }
});
