import React from "react";
import {  View, Button, StyleSheet } from "react-native";
export default function Landing({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.containerBtn}>
        <Button
          color="#f194ff"
          title="Register"
          onPress={() => navigation.navigate("Register")}
        />
        <Button title="Login" 
          onPress={() => navigation.navigate("Login")} 
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    color: 'white',
    alignItems: 'center',
    // justifyContent: "center",
  },
  containerBtn: {
    flex: 1,
    backgroundColor: 'black',
    color: 'white',
    width: '50%',
    gap : 54,
    justifyContent: "center",
  },
});
