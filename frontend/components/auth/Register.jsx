import React, { useState, useEffect } from "react";
import { Text, TextInput, View, Button, StyleSheet } from "react-native";
import { createUserWithEmailAndPassword,  updateProfile } from "firebase/auth";
import { auth, db } from "../../firebase";
import { doc, setDoc  } from "firebase/firestore"; 

export default function Register() {
  const [details, setDetails] = useState({
    email: "",
    password: "",
    name: "",
  });
  const handleChange = (e, target) => {
    setDetails({ ...details, [target]: e });
  };
  const setUser = async(userData)=>{
    try {
      const docRef = await setDoc(doc(db, "users", userData.uid), {
        name: userData.displayName,
        email: userData.email,
      });
      console.log("Document written with ID: ", docRef);
    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }
  const onSignUp = async() => {
    try {
      const { email, password, name} = details;
      let result = await createUserWithEmailAndPassword(auth, email, password).catch((err)=> console.log(err))
      console.log(result, 'this si result')
      await updateProfile(auth.currentUser, {displayName: name}).catch((err)=> console.log('error in setting display name', err))
      
      setUser(auth.currentUser);
    }
    catch (e) {
      console.log(e)
    }
    
  };
  useEffect(() => {
    console.log(auth.currentUser)
  }, [])
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Register</Text>
        <TextInput
          style={styles.textInput}
          placeholderTextColor="#e1d8d8" 
          placeholder="name"
          id="name"
          onChangeText={(e) => handleChange(e, "name")}
        />
        <TextInput
          style={styles.textInput}
          placeholderTextColor="#e1d8d8" 
          placeholder="email"
          id="email"
          onChangeText={(e) => handleChange(e, "email")}
        />
        <TextInput
          style={styles.textInput}
          placeholderTextColor="#e1d8d8" 
          placeholder="password"
          secureTextEntry={true}
          id="password"
          onChangeText={(e) => handleChange(e, "password")}
        />
      <View style={styles.viewBtn}> 
        <Button title="Sign Up" color="orange"  onPress={() => onSignUp()} />
      </View>
    </View>
  );
}
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "black", 
    color: "white"
  },
  textInput: {
    width: "80%",
    alignSelf : "center",
    margin: 2,
    borderBottomColor: '#fdfdfd',
    color: "white",
    borderBottomWidth: 1,
    marginTop: 14,
    padding: 10,
    borderRadius: 10,
    fontSize: 24,
  },
  text : {
    color: "white",
    alignSelf : "center",
    fontSize : 30,
  },
  viewBtn: {
    marginTop: 10,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
