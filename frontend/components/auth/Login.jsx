import React, { useState } from "react";
import { Text, TextInput, View, Button } from "react-native";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebase";
import { styles } from "./Register";

export default function Login() {
  const [details, setDetails] = useState({
    email: "",
    password: "",
    name : "",
  });
  const handleChange = (e, target) => {
    setDetails({ ...details, [target]: e });
  };
  const onSignUp = async() => {
    console.log("onsignup function started");
    const { email, password } = details;
    signInWithEmailAndPassword(auth, email, password)
      .then((result) => {
        console.log('user resisterd  ',result)
      })
      .catch((error) => console.log("error", error));
  };
  return (
    <View style={styles.container}>
      <Text>Register</Text>
      <TextInput
        style={styles.textInput}
        placeholder="email"
          placeholderTextColor="#e1d8d8" 
          id="email"
        onChangeText={(e) => handleChange(e, "email")}
      />
      <TextInput
        style={styles.textInput}
        placeholder="password"
          placeholderTextColor="#e1d8d8" 
          secureTextEntry={true}
        id="password"
        onChangeText={(e) => handleChange(e, "password")}
      />
      <View style={styles.viewBtn}>
        <Button
          color='green'
          title="Sign In"
          onPress={() => onSignUp()}
        />
      </View>
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: "center",
//   },
//   inputBtn: {
//     width: "50%",
//     border: "1px solid black",
//     margin: 2,
//     marginTop: 14,
//     padding: 10,
//     borderRadius: 10,
//     fontSize: 24,
//   },
//   btnBtn: {
//     marginTop: 10,
//     backgroundColor: "orange",
//   },
// });
