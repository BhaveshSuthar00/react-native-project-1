import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, Text, Button, Platform, View } from 'react-native';
import { Camera, CameraType } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';
import { useNavigation } from '@react-navigation/native';
export const Add = (props) => {
    console.log(props.route.params.type);
    const navigation = useNavigation();
    const [hasPermission, setHasPermission] = useState(null);
    const [camera, setCamera] = useState(null);
    const [image, setImage] = useState(null);
    const [type, setType] = useState(CameraType.back);
    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.cancelled) {
        setImage(result.uri);
        }
    };
    const takePicture = async () => {
        if(camera){
            const data = await camera.takePictureAsync(null);
            console.log(data.uri)
            setImage(data.uri);
        }
    }
    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null || Platform.OS === 'web') {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }
    return (
        <View style={styles.container}>
        <View 
            style={styles.cameraContainer}
        >
            <Camera ref={ref => setCamera(ref)} style={styles.fixedRatio} type={type} />
        </View>
        {image && <Image  source={{uri : image}} style={{flex : 1}}/>}
        <Button
            title="Flip Image"
            style={styles.button}
            onPress={() => {
            setType(type === CameraType.back ? CameraType.front : CameraType.back);
            }} />
        <Button 
            title='Take Picture'
            onPress={() => takePicture()}
        />
        <Button title="Pick an image from camera roll" onPress={pickImage} />
        <Button title='Save' onPress={() => navigation.navigate('Save', {image, type : props.route.params.type})} />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    cameraContainer : {
        flex : 1,
        flexDirection: 'row',
    },
    fixedRatio : {
        flex: 1,
        aspectRatio : 1
    },
    camera: {
    }, 
    button : {
        fontSize : "34px"
    }
});