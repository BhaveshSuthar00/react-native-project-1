import React, { useEffect, useState } from 'react'
import {View, Image, Text, Dimensions, StyleSheet, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import ImageSlider from 'react-native-image-slider';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const StoriesSingle = (props) => {
    const [stories, setStories] = useState([])
    useEffect(()=>{
        const storiesIn = props.route.params.collection;
        const userId = props.route.params.uid;
        const collectionArr = [];
        for(let i = 0; i <storiesIn.length; i++){
            if(storiesIn[i].uid === userId){
                collectionArr.push(storiesIn[i]);
            }
        }
        console.log(collectionArr)
        setStories(collectionArr)
        return(()=>{
            setStories([])
        })
    },[])
    if(stories.length === 0){
        return <View>
            <Text>Loading....</Text>
        </View>
    }
    return (
        <View style={styles.container}>
        <ImageSlider
            loopBothSides
            autoPlayWithInterval={3000}
            images={stories}
            customSlide={({ index, item, style, width }) => (
                // It's important to put style here because it's got offset inside
                <View key={item.id} style={[style, styles.containerImage]}>
                <Image source={{ uri: item.downloadURL }} style={styles.image} />
                </View>
            )}
            
        />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerImage : {
        flex: 1,
    },
    image : {
        // flex: 1,
        width: windowWidth,
        height: windowHeight-70,
        overflow: 'hidden',
        borderWidth: 1,
        // aspectRatio : 1/1,
    }
});
  
export default StoriesSingle