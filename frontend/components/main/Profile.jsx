import { collection, doc, getDoc, getDocs, query, where, setDoc, Timestamp, deleteDoc } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { View, Text, Platform, StatusBar, Button, Image, FlatList, StyleSheet } from 'react-native';
import { connect, useDispatch, useSelector } from 'react-redux'
import { auth, db } from '../../firebase';
import { fetchUserFollowing, fetchUserPosts, LogOut } from '../../redux/actions';
const Profile = (props) => {
    const {currentUser, posts, following} = props;
    // const {currentUser, posts, following} = useSelector((store)=> store.userState);
    const [followingr, setFollowing] = useState(false);
    const [userPost, setUserPosts] = useState([]);
    const [user, setUser] = useState(null);
    const dispatch = useDispatch();
    const fetchData = async()=> {
        const q = doc(db, "users", props.route.params.uid);
        const querySnapshot = await getDoc(q);
        const id= querySnapshot.id
        setUser({
            id,
            ...querySnapshot.data(),
        })
        const allPosts = await getDocs(collection(db, "posts", props.route.params.uid, "userPosts"));
            const arr = [];
            allPosts.forEach((doc) => { 
                const data = doc.data();
                const id = doc.id;
                arr.push({id , ...data})
            })
            setUserPosts(arr)
    }
    const onFollow = async() => {
        try {
            const q = doc(db, "following", auth.currentUser.uid, "userFollowing", props.route.params.uid);
            await setDoc(q, {})
            // dispatch(fetchUserFollowing())
            setFollowing(!followingr)
        }
        catch(err) {
            alert(err.message)
        }
    }
    
    const onUnFollow = async() => {
        try {
            const q = doc(db, "following", auth.currentUser.uid, "userFollowing", props.route.params.uid);
            await deleteDoc(q, {})
            // dispatch(fetchUserFollowing())
            setFollowing(!followingr)
        }
        catch (err) {
            alert(err.message)
        }
    }
    useEffect(()=>{
        if(props.route.params.uid === auth.currentUser.uid){
            setUser(currentUser);
            // console.log(posts, 'insdie profile')
            setUserPosts(posts)
        } else {
            fetchData();
            if(following.indexOf(props.route.params.uid) > -1){
                dispatch(fetchUserFollowing())
                setFollowing(true)
            } else if(following.indexOf(props.route.params.uid) && props.route.params.uid !== auth.currentUser.uid) {                 
                dispatch(fetchUserFollowing())
                setFollowing(false)
            }
        }
    },[props.route.params.uid])
    if(currentUser === null || posts === null || user === null ){
        return <View />
    }
    return (
        <View style={styles.container}>
            <View style={styles.textContainer}>
                <Text style={styles.text}>
                    Profile {user.displayName}
                </Text>
                <Text style={styles.text}>
                    Profile {user.email}
                </Text>
                {props.route.params.uid !== auth.currentUser.uid && !followingr  ? (
                    <View>
                        <Button title="Follow" onPress={()=> onFollow()}/>
                    </View>
                ) : null}
                {props.route.params.uid !== auth.currentUser.uid && followingr  ? (
                    <View>
                            <Button title="Following" onPress={()=> onUnFollow()}/>
                        </View>
                    
                ) : null}
                {props.route.params.uid === auth.currentUser.uid ? 
                    <View>
                        <Button title="Log Out" onPress={()=>  dispatch(LogOut())}/>
                    </View> : null
                }
            </View>
            <View style={styles.containerGallery}>
                <FlatList 
                    numColumns={3}
                    horizontal={false}
                    data={userPost}
                    renderItem={({item})=>{
                        return (
                            <View style={styles.containerImage}>
                                <Image 
                                    style={styles.image}
                                    source={{uri : item.downloadURL}}
                                />
                            </View>
                        )
                    }}
                />
                {userPost.length === 0 && (
                    <View>
                        <Text>No Posts</Text>
                    </View>
                )}
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: "center",
        paddingTop : Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        backgroundColor : 'black',
    },
    textContainer : {
        backgroundColor : "black",
    },
    text : {
        color: 'white',
        marginTop : 4,
    },
    containerGallery : {
        flex : 1,
        backgroundColor : 'black'
    },
    containerImage : {
        flex: 1,
        marginTop : 15,
        margin: 3
    },
    image : {
        aspectRatio : 1/1
    }
});
const mapStateToProps = (store) => ({
    currentUser: store.userState.currentUser,
    posts : store.userState.posts,
    following : store.userState.following
})
// export default Profile;
export default connect(mapStateToProps, null)(Profile)