import React from 'react'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {View, Text, StyleSheet, TouchableOpacity, Image, FlatList} from 'react-native'
import { useNavigation } from '@react-navigation/native';
export const Stories = (props) => {
    const { stories } = props;
    const navigation = useNavigation();
    if(stories.length === 0) {
        return (
            <View style={styles.container}>
                            
                <TouchableOpacity onPress={()=> navigation.navigate('MainAdd', {type : "Story"})}>
                    <MaterialCommunityIcons name='account-circle' size={58} />
                </TouchableOpacity>
            </View>
        )
    }
    return (
        <View style={styles.container2}>
            <TouchableOpacity onPress={()=> navigation.navigate('MainAdd', {type : "Story"})}>
                <MaterialCommunityIcons name='account-circle' color='white' size={58} />
            </TouchableOpacity>
            
            <FlatList 
            numColumns={3}
            horizontal={false}
            data={stories}
            renderItem={({item})=>{
                return (
                    <View style={styles.containerImage} >
                        <TouchableOpacity 
                            onPress={()=> navigation.navigate('Stories', {collection : stories, uid : item.uid})}
                        >
                            <Image 
                                style={styles.image}
                                source={{uri : item.downloadURL}}
                            />
                        </TouchableOpacity>
                    </View>
                )
            }}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: 60,
        backgroundColor: 'white',
    },
    container2 : {
        height: 60,
        justifyContent : 'center',
        flexDirection: 'row',
    },
    containerImage : {
        height: 60,
        justifyContent : 'center',
        marginLeft: 10,
    },
    image : {
        // flex: 1,
        width: 54,
        height: 54,
        borderRadius: 50,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: 'red',
        // aspectRatio : 1/1,
    }
})