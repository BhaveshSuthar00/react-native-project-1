import { useNavigation } from '@react-navigation/native';
import { deleteDoc, doc, setDoc } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { Button, View, Text, StyleSheet, StatusBar, Image, FlatList, Platform} from 'react-native';
import { useDispatch, connect } from 'react-redux';
import { auth, db } from '../../firebase';
import { Stories } from './Stories';
const Feed = (props) => {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [posts, setPosts] = useState([]);
    const onUnlike = async(postId, uid)=>{
        try {
            // const q = query(collection(db, "posts", uid, "userPosts", postId, "likes"));
            const q = doc(db, "posts", uid, "userPosts", postId, "likes", auth.currentUser.uid);
            await deleteDoc(q, {})
        }
        catch (err) {
            alert(err.message)
        }
    }
    const onLike = async(postId, uid)=>{
        try {
            // const q = query(collection(db, "posts", uid, "userPosts", postId, "likes"));
            const q = doc(db, "posts", uid, "userPosts", postId, "likes", auth.currentUser.uid);
            await setDoc(q, {});
        }
        catch(err) {
            alert(err.message, 'eorr in feed.jsx')
        }
    }
    useEffect(()=>{
        if(props.usersLoaded === props.following.length || props.following.length !== 0) {
            setPosts(props.feed);
        }
        // console.log(props.feed, 'feed changed');
        return(()=>{
            setPosts([]);
        })
    },[props.usersLoaded, props.feed])
    if(posts.length === 0) {
        return <View />
    }
    return (
        <View style={styles.container}>
            <Stories stories={props.stories}/>
            <View style={styles.containerGallery}>
                <FlatList 
                    numColumns={1}
                    horizontal={false}
                    data={posts}
                    renderItem={({item})=>{
                        return (
                            <View style={styles.containerImage}>
                                <Image 
                                    style={styles.image}
                                    source={{uri : item.downloadURL}}
                                />
                                <Text
                                    style={styles.text}
                                    onPress={()=> navigation.navigate('Comment', {postId : item.id, uid : item.uid})}
                                >View Comments...</Text>
                                {
                                    item.currentUserLike ? (

                                        <Button title="DisLike" onPress={()=> onUnlike(item.id, item.uid)}/> 
                                    ) : (
                                        <Button title="Like" onPress={()=> onLike(item.id, item.uid)}/> 
                                    )
                                }
                            </View>
                        )
                    }}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: "center",
        paddingTop : Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        backgroundColor : 'black',
    },
    textContainer : {
        backgroundColor : "black",
    },
    text : {
        color: 'white',
        marginTop : 4,
    },
    containerGallery : {
        flex : 1,
        backgroundColor : 'black'
    },
    containerImage : {
        flex: 1,
        marginTop : 15,
        margin: 3
    },
    image : {
        aspectRatio : 1/1
    }
});
const mapStateToProps = (store) => ({
    // currentUser: store.userState.currentUser,
    // posts : store.userState.posts,
    following : store.userState.following,
    feed : store.users.feed,
    users : store.users.usersList,
    usersLoaded : store.users.userLoaded,
    stories : store.users.stories
})
// export default Profile;
export default connect(mapStateToProps, null)(Feed)