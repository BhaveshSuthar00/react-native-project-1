import { addDoc, collection, doc, onSnapshot, query } from 'firebase/firestore';
import React, { useState, useEffect } from 'react'
import { View, TextInput, Button, FlatList, Text } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { auth, db } from '../../firebase';
import { fetchUserData } from '../../redux/actions/index'
export const Comment = (props) => {
    const [comments, setComments] = useState([]);
    const { usersList } = useSelector((store)=> store.users)
    const [postId, setPostId] = useState("");
    const [text, setText] = useState('');
    const dispatch = useDispatch();
    const matchUserToComments = (comments)=>{
        for(let i = 0; i<comments.length; i++) {
            if(comments[i].creator === auth.currentUser.uid){
                comments[i].userName = auth.currentUser.displayName;
            } else if(comments[i].creator !== auth.currentUser.uid){
                let flag = false;
                usersList.map((item)=>{
                    if(item.id === comments[i].creator){
                        flag = true;
                        comments[i].userName = item.name;
                    }
                })
                if(!flag){
                    console.log('in flag false state')
                    dispatch(fetchUserData(comments[i].creator))
                    usersList.map((item)=>{
                        if(item.id === comments[i].creator){
                            console.log('user is added')
                            comments[i].userName = item.name;
                        }
                    })
                }
            }
            setComments(comments)
        }
    }
    const onCommentSend = async()=>{
        try {
            const q = collection(db, "posts", props.route.params.uid, "userPosts", props.route.params.postId, "comments"
            );
            if(text.length > 0){
                addDoc(q , {
                    creator: auth.currentUser.uid, 
                    text : text || "",
                })
            }
        }
        catch (e) {
            console.log(e, 'error in comment compnent in onCommentSend function');
        }
    };
    // console.log(props.route.params.uid is the user id and id is post id)
    const fetchData = async()=> {
        try {
            const q = query(collection(db, "posts", props.route.params.uid, "userPosts", 
            props.route.params.postId, "comments"
            ));
        onSnapshot(q, (res)=>{
            let arr = [];
            res.forEach((all)=>{ 
                const data = all.data();
                const id = all.id;
                arr.push({id, ...data})
            })
            matchUserToComments(arr)
        });
        }
        catch (e) {
            console.log(e, 'error in comments component fetchData function')
        }
    }
    useEffect(()=>{
        if(props.route.params.postId !== postId) {
            fetchData()
        }
    },[props.route.params.postId])
    useEffect(()=>{
        console.log(comments)
    },[comments])
    return (
        <View>
            <FlatList 
                numColumns={1}
                horizontal={false}
                data={comments}
                renderItem={({item})=>{
                    return (
                        <View>
                            <Text>{item.userName}</Text>
                            <Text>{item.text}</Text>
                        </View>
                    )
                }}
            />
            <View>
                <TextInput  onChangeText={(item)=> setText(item)}/>
                <Button title="Save" onPress={()=> onCommentSend()} />
            </View>
        </View>
    )
}