import React, { useState } from 'react'
import { View, Text, TextInput, FlatList, TouchableOpacity, StyleSheet, Platform, StatusBar } from 'react-native'
import { collection, query, where, getDocs } from "firebase/firestore";
import { db } from '../../firebase';
import { useNavigation } from '@react-navigation/native';
const Search = () => {
    const [users, setUsers] = useState([]);
    const navigation = useNavigation();
    const fetchUsers = async(search)=> {
        setUsers([])
        const q = query(collection(db, "users"), where("name", ">=", search));
        const querySnapshot = await getDocs(q);
        const arr = [];
        querySnapshot.forEach((doc) => {
            const id = doc.id;
            arr.push({id, ...doc.data()});
        });
        setUsers(arr);
    }
    return (
        <View style={styles.container}>
            <TextInput
            style={styles.textInput}
            placeholderTextColor="#e1d8d8" 
            placeholder="search"  
            onChangeText={(search)=> fetchUsers(search)}/>
            <FlatList 
                numColumns={1}
                horizontal={false}
                data={users}
                renderItem={({item})=> (
                    <TouchableOpacity
                        onPress={()=> navigation.navigate('Profile', { uid : item.id })}
                    >
                        <Text style={styles.text}>{item.name}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "black",
        marginTop : Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        color: "white"
    },
    text : {
        color: "white",
        fontSize: 22,
        alignItems: "center"
    },
    textInput : {
        alignSelf: "center",
        textAlign: "center",
        color: "white",
        borderBottomWidth: 3,
        borderBottomColor: '#fdfdfd',
        marginTop: 14,
        padding: 10,
        borderRadius: 10,
        fontSize: 24,
    },
})
export default Search