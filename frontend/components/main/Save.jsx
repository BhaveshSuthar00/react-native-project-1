import React, { useEffect, useState } from 'react';
import { View, TextInput, Image, Button } from "react-native";
import { getStorage, ref, uploadBytes , uploadBytesResumable, getDownloadURL} from "firebase/storage";
const storage = getStorage();
import { doc, Timestamp, setDoc, serverTimestamp, updateDoc } from "firebase/firestore";
import { auth, db } from "../../firebase";
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
export const Save = (props) => {
    const dispatch = useDispatch()
    const [uploading, setUploading] = useState(false);
    const navigation = useNavigation();
    const [caption, setCaption] = useState("");
    const uploadImage = async () => {
        const uri = props.route.params.image;
        const response = await fetch(uri);
        const blob = await response.blob();
        const metadata = {
            contentType: 'image/jpeg'
        };
        // Upload file and metadata to the object 'images/mountains.jpg'
        let path;
        if(props.route.params.type === 'Post'){
            path = `posts/${auth.currentUser.uid}/${Math.random().toString(36)}`;
        } else {
            path = `stories/${auth.currentUser.uid}/${Math.random().toString(36)}`;
        }
        console.log(path, 'path of image repo', props.route.params.type)
        const storageRef = ref(storage, path);
        const uploadTask = uploadBytesResumable(storageRef, blob, metadata);
        setUploading(true)
        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on('state_changed', (snapshot) => {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            switch (snapshot.state) {
            case 'paused':
                console.log('Upload is paused');
                break;
            case 'running':
                console.log('Upload is running');
                break;
            }
        },
        (error) => {
            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
                case 'storage/unauthorized':
                    console.log('storage/unauthorized' ,error)
                break;
                case 'storage/canceled':
                    console.log('storage/canceled' ,error)
                break;
                    // ...
                case 'storage/unknown':
                    console.log('storage/unknown' ,error)
                break;
            }
    },
    () => {
        // Upload completed successfully, now we can get the download URL
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            if(props.route.params.type === 'Story'){
                saveStory(downloadURL)
            } else {
                savePostData(downloadURL)
            }
        });
    }
    );
};
const saveStory = async(downloadURL) => {
    try {
        const docRef = doc(db, "posts", auth.currentUser.uid, 'userStories', Math.random().toString(36))
        await setDoc(docRef, {
            downloadURL : downloadURL || "No url",
            caption : caption === '' ? 'No caption' : caption,
            dataExample : "",
        })
        await updateDoc(docRef, {
            dataExample: serverTimestamp()
        });
        console.log("Upload stories")
        setUploading(false);
        // dispatch(fetchUserFollowing())
        navigation.navigate('Feed')
    }
    catch (error){
        alert(error.message)
    }
}
const savePostData = async (downloadURL) => {
    try {
        const docRef = doc(db, "posts", auth.currentUser.uid, 'userPosts', Math.random().toString(36))
        await setDoc(docRef, {
            downloadURL : downloadURL || "No url",
            caption : caption === '' ? 'No caption' : caption,
            dataExample : "",
        })
        const updateTimestamp = await updateDoc(docRef, {
            dataExample: serverTimestamp()
        });
        console.log("post Upload")
        setUploading(false);
        // dispatch(fetchUserFollowing()) commented at 2 pm today
        navigation.navigate('Profile', {uid : auth.currentUser.uid})
    } catch (e) {
        alert(e.message)
    }
};
return (
    <View style={{ flex: 1 }}>
    <Image source={{ uri: props.route.params.image }} />
    <TextInput
        placeholder="Write a Caption ..."
        onChangeText={(caption) => setCaption(caption)}
    />
    {!uploading && (
        <Button title="Save" onPress={()=> uploadImage()} />
    )}
    </View>
);
};